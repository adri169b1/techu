//version inicial

var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
var requestjson = require('request-json');
var path = require('path');
var basemlabURL = "https://api.mlab.com/api/1/databases/lmorales/collections/";
var apikeymlab = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usersFile = require('./usuarios.json');

//var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/lmorales/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
//var clienteMLab = requestjson.createClient(urlmovimientosMLab);

app.listen(port, function(){
  console.log("todo list RESTful API server started on: " + port+ "....");
});

app.use(bodyParser.json());

app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,Accept");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});


// GET users -- retorna todods los usuarios de la bd coleccions usuarios
app.get('/usuarios',function(req,res){
  var clienteMLab = requestjson.createClient(basemlabURL);
  clienteMLab.get("usuarios?"+ apikeymlab, function(err, resM, body){
    if(err) {
      console.log(body);
    }else {
      res.send(body);
    }
  });

});

// GET users -- retorna todods los pagos  de la bd coleccions facturas
app.get('/facturas',function(req,res){
  var clienteMLab = requestjson.createClient(basemlabURL);
  clienteMLab.get("facturas?"+ apikeymlab, function(err, resM, body){
    if(err) {
      console.log(body);
    }else {
      res.send(body);
    }
  });

});
//peticion post para login con mlab email y password
  app.post('/usuarios'+'/ingreso',function(req, res) {
     console.log("Email body" + req.body.email);
     console.log(req.body.password);
     var resemail = "";
      var pass = req.body.password;
     var respass = req.body.password;
     var email = req.body.email;
     //var email = "linamor@hotmail.com";
      //var pass = "Lins12";
     var filtro = 'f={"_id": 0}&';
     var id = "";
     var fn = "";
     var ln="";
     var queryString = 'q={"email":"' + email +'"}&';
     var clienteMLab = requestjson.createClient(basemlabURL);

     clienteMLab.get("usuarios?" +filtro  + queryString + apikeymlab, function(err, resM, body)
     {
      console.log("error = "+ err);
      console.log("query = "+ queryString);
      console.log("body = " + body[0]);
      console.log("res mlab = " + resM);
      if(Object.keys(body).length > 0 ){
       resemail = body[0].email;
       respass = body[0].password;
       id = body[0].id;
       fn= body[0].first_name;
       ln= body[0].last_name;
       console.log("pass de body : "+ body[0].password);
       console.log("email de body : "+ body[0].email);
       console.log(" fn = " + fn);
       console.log(" ln = " + ln);
     }
       if(resemail == email){
         if(respass == pass){
          var clienteMLab = requestjson.createClient(basemlabURL);
           var cambio = '{ "$set":' + JSON.stringify({"logged":true}) + '}';
           clienteMLab.put("usuarios?"+'?q={"id": ' + id + '}&' + apikeymlab, JSON.parse(cambio), function(err, resM, body) {
             console.log("error = " + err);
             console.log("resM = " + resM);
      //       res.send({"msg":"200","id":id,"fn":fn,"ln":ln});
            res.send("200");
           });
         }else{
             res.send({"msg":"400 Password no válido"});
         }
       }else{
         res.send({"msg":"401 Email no existe"});
       }
     });
  });


  // POST dar de alta un usuario
  app.post('/usuarios'+'/registro',function(req, res) {
  console.log("req body"+ req.body);
  var clienteMlab = requestjson.createClient(basemlabURL);
  clienteMlab.post( "usuarios?"+ apikeymlab, req.body, function(err, resM, body) {
    console.log("err = " + err);
    console.log("Nombre post" + req.body.nombre);
    console.log("Apellido post" + req.body.apellido);
    console.log("Cedula post" + req.body.cedula);
    console.log("Email post" + req.body.email);
    console.log("Password post" + req.body.password);
    //res.send(req.body);

    if (err == null){
      res.send("200");
    }else {
       res.send({"msg":"Su Registro no pudo completarse"});
    }
  });
  });

// Petición GET a facturas con con email y todas las referencias en mlab
app.get( '/facturas/:email/referencia/',function (req, res) {
   var filtro = 'f={"_email": 0}&';
   console.log("filtro" + filtro);
  console.log("GET facturas/:email/referencia/");
  console.log('req.params.email: ' + req.params.email);
  var email = req.params.email;
  var respuesta = req.params;
  var queryString = 'q={"email":"' + email +'"}&';
    var clienteMLab = requestjson.createClient(basemlabURL);
        clienteMLab.get("facturas?" +filtro  + queryString + apikeymlab, function(err, resM, body)
        {
      console.log("respuesta mlab correcta");
      var respuesta = body;
      res.send(respuesta);
   });
});
// Petición GET a facturas por referencias en mlab
app.get('/facturas/:referencia/',function (req, res) {
  var filtro = 'f={"_referencia": 0}&';
  console.log("GET /facturas/referencia");
  console.log('req.params.referencia: ' + req.params.referencia);
  var id = req.params.referencia;
  var respuesta = req.params;
  var queryString = 'q={"referencia":'+ id + '}&';
  var httpClient = requestjson.createClient(basemlabURL);
  httpClient.get ('facturas?' + filtro + queryString + apikeymlab,
   function(err, respuestaMlab, body)
    {
      console.log("respuesta mlab correcta");
      var respuesta = body;
      res.send(respuesta);
   });
});

// POST dar de alta facturas
app.post('/facturas'+'/registro',function(req, res) {
console.log("req body"+ req.body);
var clienteMlab = requestjson.createClient(basemlabURL);
clienteMlab.post( "facturas?"+ apikeymlab, req.body, function(err, resM, body) {
  console.log("err = " + err);
  console.log("referencia" + req.body.referencia);
  console.log("Valor" + req.body.valor);
  console.log("Fecha_Vec" + req.body.fecha_Vec);
  console.log("Empresa" + req.body.empresa);
  console.log("Estado" + req.body.estado);
  console.log("Email post" + req.body.email);
  //res.send(req.body);
  if (err == null){
    res.send("200");
  }else {
     res.send({"msg":"Su Registro no pudo completarse"});
  }
});
});

//Actualizar el estado de la factura
app.put('/facturas/:referencia', function(req, res) {
  console.log("url facturas/:referencia");
    var referencia = req.params.referencia;
    var estadonew  = req.body.estado;

    console.log("nuevo estado" + estadonew + referencia);
  //var referencia ="1230001";
  var clienteMlab = requestjson.createClient(basemlabURL + "/facturas?")
           var cambio = '{"$set":' + JSON.stringify({"estado":estadonew}) + '}';
           clienteMlab.put('?q={"referencia": ' + referencia + '}&' + apikeymlab, JSON.parse(cambio), function(err, resM, body) {
             console.log("error = " + err);
             console.log("resM = " + resM);
             res.send({"msg":"referencia cambiada -2"});
           });
        });
// final pub con mlab
